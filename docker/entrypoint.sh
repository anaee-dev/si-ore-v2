#!/bin/sh

# Trim leading and trailing whitespace from the ( backend_url) input
BACKEND_URL=$(echo "$1" | awk '{$1=$1;print}')

# Check if the trimmed input is not empty
if [ -n "$BACKEND_URL" ]; then
  # Search for the pattern: SERVER = window.location.origin 
  # And replace only 'window.location.origin' by $BACKEND_URL variable
  echo "Replacing window.location.origin with $BACKEND_URL in HTML files..."
  find /usr/share/nginx/html -type f -exec sed -i "s|SERVER\s*=\s*window\.location\.origin|SERVER = \"$BACKEND_URL\";|g" {} +

else
  echo "No BACKEND_URL provided. Default backend url : window.location.origin"
fi

# Start Nginx
echo "Starting Nginx.."
nginx -g 'daemon off;'
