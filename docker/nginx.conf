# /etc/nginx/nginx.conf

user  nginx;
worker_processes auto;

error_log /var/log/nginx/error.log warn;
 
pid       /var/run/nginx.pid;

events {
    worker_connections 1024;
}

http {
    include       /etc/nginx/mime.types;
    default_type  application/octet-stream;

    log_format main '$remote_addr - $remote_user [$time_local] "$request" '
                    '$status $body_bytes_sent "$http_referer" '
                    '"$http_user_agent" "$http_x_forwarded_for"';

    access_log /var/log/nginx/access.log main;

    sendfile           on;
    keepalive_timeout  65;

    client_max_body_size 900M ;

    ## Manage timeout 
    proxy_connect_timeout 86400s;
    proxy_send_timeout 86400s;
    proxy_read_timeout 86400s;
    send_timeout 86400s;

    # Server block configuration
    server {
        listen       80;
        server_name  localhost;  # domain name

        root   /usr/share/nginx/html;  # static pages
        index  index.html;

        location / {
            # Try to find the file or redirect to 
            # index.html for client-side routing
            try_files $uri $uri/ /index.html;
        }

        # Handle 404 errors
        error_page 404 /index.html;

        # Configure cache headers to improve performance
        location ~* \.(js|css|png|jpg|jpeg|gif|ico|svg)$ {
            expires 1d;
            access_log off;
        }

        # Configure error log file
        error_page 500 502 503 504 /50x.html;
        location = /50x.html {
            root /usr/share/nginx/html;
        }
    }
}
