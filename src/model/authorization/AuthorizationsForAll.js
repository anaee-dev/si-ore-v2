export class AuthorizationsForAll {
  dependantNodes = null;
  dependsOf = null;
  constructor(dependantNodes, dependsOf, authorizations) {
    this.dependantNodes = dependantNodes;
    this.dependsOf = dependsOf;
    this.authorizations = authorizations;
  }
}
