export class ComponentKey {
  variable;
  component;
  constructor(variableOrComponentKey, component) {
    if (typeof variableOrComponentKey == "object") {
      Object.keys(this).forEach(
        (key) => (this[key] = variableOrComponentKey[key] ? variableOrComponentKey[key] : null)
      );
    } else {
      this.variable = variableOrComponentKey;
      this.component = component;
    }
  }
}
