import { IntervalValues } from "./IntervalValues";
import { ComponentKey } from "./ComponentKey";

export class ComponentFilters {
  componentKey;
  filter;
  type;
  format;
  intervalValues;
  isRegExp = false;
  constructor(componentFiltersOrComponentKey, filter, intervalValues, isRegExp) {
    if (typeof componentFiltersOrComponentKey != "string") {
      Object.keys(this).forEach(
        (key) =>
          (this[key] = componentFiltersOrComponentKey[key]
            ? componentFiltersOrComponentKey[key]
            : null)
      );
    } else {
      this.componentKey = new ComponentKey(componentFiltersOrComponentKey);
      this.filter = filter;
      this.intervalValues = new IntervalValues(intervalValues);
      this.isRegExp = isRegExp == null ? false : isRegExp;
    }
  }
}
