export class ComponentOrderBy {
  componentKey;
  order;
  type;
  format;
  static ORDER() {
    return ["ASC", "DESC"];
  }

  constructor(componentOrderByOrComponentKey, order, type, format) {
    if (typeof componentOrderByOrComponentKey == "object") {
      Object.keys(this).forEach(
        (key) =>
          (this[key] = componentOrderByOrComponentKey[key]
            ? componentOrderByOrComponentKey[key]
            : null)
      );
    } else {
      this.componentKey = componentOrderByOrComponentKey;
      this.order = this.ORDER.includes(order) ? order : null;
      this.type = type;
      this.format = format;
    }
  }
}
