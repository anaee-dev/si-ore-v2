const { defineConfig } = require("cypress");

module.exports = defineConfig({
  env: {
    home_url: "/",
    login_url: "/login",
    applications_url: "/applications",
    applicationCreation_url: "/applicationCreation",
    monsore_new_authorization_url: "/applications/monsore/authorizations/new",
    monsore_table_authorization_url: "/applications/authorizations",
    dataType_monsore_url: "/applications/monsore/dataTypes",
    repository_foret_url: "/applications/foret/dataTypesRepository/swc_j",
    zone_etude_url: "applications/foret/references/zones_etudes.js",
    ola_references_url: "/applications/ola/references",
    monsore_references_url: "/applications/monsore/references",
    ola_authorization_references_url:
      "/applications/ola/references/authorizations",
    ola_new_authorization_references_url:
      "/applications/ola/references/authorizations/new",
    ola_dataTypes_url: "/applications/ola/dataTypes",
    ola_dataTypes_authorizations_url: "/applications/ola/authorizations",
    ola_dataTypes_new_authorizations_url:
      "/applications/ola/authorizations/new",
    ola_dataTypes_update_authorizations_url:
      "/applications/ola/authorizations/c858e98b-a60e-4ee8-ae20-1f6814a7e77f",
  },

  videoUploadOnPasses: false,
  video: true,
  videoCompression: false,

  e2e: {
    // We've imported your old cypress plugins here.
    // You may want to clean this up later by importing these.
    setupNodeEvents(on, config) {
      return require("./cypress/plugins/index.js")(on, config);
    },
    baseUrl: "http://localhost:8080",
  },

  component: {
    devServer: {
      framework: "vue-cli",
      bundler: "webpack",
    },
  },
});
